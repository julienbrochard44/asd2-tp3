#include "anneau.hpp"

using namespace std;

int main() {
    Anneau<int> anneau;
    anneau.ajouter(45);
    anneau.ajouter(42);

    cout << anneau.lire() << endl;
    cout << anneau.taille() << endl;
    
    return 0;
}