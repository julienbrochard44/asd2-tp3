#include <iostream>
#include <list>
#include <cassert>

/***************************************
 * Classe Anneau générique
 */
template<typename T>
class Anneau {
    public:
    // documentation des méthodes avec leurs définitions ci-dessous
        Anneau();
        ~Anneau();
        size_t taille() const;
        void ajouter(T);
        void retirer();
        T lire() const;
        void ecrire(T);
        void avancer();
        void reculer();
    private:
    // implémenté par une std::list
        std::list<T> _elements;
        typename std::list<T>::iterator _tete;
};

/***************************************
 * Constructeur
 */
template<typename T>
Anneau<T>::Anneau() {
	_tete = _elements.begin();
}

/***************************************
 * Destructeur
 */
template<typename T>
Anneau<T>::~Anneau() {
    // TODO
}

/***************************************
 * Taille, i.e., nombre d'éléments
 * @pre aucune
 */
template<typename T>
size_t Anneau<T>::taille() const {
    return _elements.size();
}

/***************************************
 * Insérer un nouvel élément t par décalage
 * @pre aucune
 */
template<typename T>
void Anneau<T>::ajouter(T t) {
	_elements.push_back(t);
	_tete = _elements.end();
}

/***************************************
 * Supprimer l'élément courant par décalage
 * @pre anneau non vide
 */
template<typename T>
void Anneau<T>::retirer() {
	// beta
    _elements.erase(_tete);
}

/***************************************
 * Retourne l'élément pointé par la tête
 * @pre anneau non vide
 */
template<typename T>
T Anneau<T>::lire() const {
    return *_tete;
}

/***************************************
 * Modifie l'élément pointé par la tête
 * @pre anneau non vide
 */
template<typename T>
void Anneau<T>::ecrire(T t) {
    // TODO
}

/***************************************
 * Déplace la tête sur l'élément suivant
 * @pre anneau non vide
 */
template<typename T>
void Anneau<T>::avancer() {
    _tete++;
}

/***************************************
 * Déplace la tête sur l'élément précédent
 * @pre anneau non vide
 */
template<typename T>
void Anneau<T>::reculer() {
    // TODO
}

/***************************************
 * Fonction d'affichage dans un flux de sortie
 */
template<typename T>
std::ostream & operator<<(std::ostream &os, Anneau<T> &a) {
    size_t i;
    os << "(" << a.taille() << ")[ ";
    for (i = 0 ; i < a.taille() ; i++) {
        os << a.lire() << " ";
        a.avancer();
    }

    os << "]";
    return os;
}
